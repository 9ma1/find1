OPEN "input.txt" FOR INPUT AS #1
INPUT #1, A$
PRINT A$
n = LEN(A$)
q = 0
maxq = 0

FOR i = 1 TO n STEP 1
    IF MID$(A$, i, 1) = "1" THEN
        q = q + 1
    ELSE
        IF q > maxq THEN
            maxq = q
        END IF
        q = 0
    END IF
NEXT i
IF maxq > q THEN
    PRINT maxq
    OPEN "output.txt" FOR OUTPUT AS #2
    PRINT #2, maxq
ELSE
    PRINT q
    OPEN "output.txt" FOR OUTPUT AS #2
    PRINT #2, q

END IF

